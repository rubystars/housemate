class GroupsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_group, only: [:show, :edit, :update, :destroy]
  before_action :set_house

  def index
    @groups = Group.all
    #authorize! :index, @groups -- error, does not allow for anybody to show all groups
  end

  def show
    authorize! :show, @group
  end

  def new
    @group = Group.new
    #authorize! :new, @group -- to be fixed, doesn't work, nobody can create new groups
  end

  def edit
    authorize! :edit, @group
  end

  def create
    @group = Group.new(group_params)
    @house.groups << @group
    current_user.groups << @group

    respond_to do |format|
      if @group.save
        format.html { redirect_to house_group_url(@house, @group), notice: 'Group was successfully created.' }
        format.json { render :show, status: :created, location: @group }
      else
        format.html { render :new }
        format.json { render json: @group.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @group.update(group_params)
        format.html { redirect_to house_group_url(@house, @group), notice: 'Group was successfully updated.' }
        format.json { render :show, status: :ok, location: @group }
      else
        format.html { render :edit }
        format.json { render json: @group.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @group.destroy
    respond_to do |format|
      format.html { redirect_to house_groups_url, notice: 'Group was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  def set_group
    @group = Group.find(params[:id])
  end

  def set_house
    @house = House.find(params[:house_id])
  end

  def group_params
    params[:group].permit(:name, :about)
  end
end
